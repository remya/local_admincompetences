<?php
/**
 * @category UCN
 * @package  admincompetences
 *
 * @author   CEMU UCN  - Anne Garnavault - anne.remy@unicaen.fr
 * @license  http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @link
 */


/**
 * Add a link in settings for selected users.
 *
 * @param array     $settings
 * @param object    $context
 * @return void
 */
 
function local_admincompetences_extend_settings_navigation($settingsnav, $context) {
    global $CFG, $PAGE;
	// Only add this settings item on non-site course pages.
    if (!$PAGE->course or $PAGE->course->id == 1) {
        return;
     
    }
  
    // Only let users with the appropriate capability see this settings item.
   if (!has_capability('moodle/competency:coursecompetencymanage', context_course::instance($PAGE->course->id))) {
        return;
    }

    // IF we're viewing course and the course is not the front page
    // add the link 
   
 
  if ($settingnode = $settingsnav->find('courseadmin', navigation_node::TYPE_COURSE)) 
	{
	  $strfoo = get_string('competencies', 'competency');
	  $url = new moodle_url('/admin/tool/lp/coursecompetencies.php',array('courseid' => $PAGE->course->id));
	  
      $foonode = navigation_node::create(
            $strfoo,
            $url,
            navigation_node::NODETYPE_LEAF,
            'admincompetences',
            'admincompetences'
            ,   new pix_icon('i/competencies', $strfoo)
        );   

      if ($PAGE->url->compare($url, URL_MATCH_BASE)) {
            $foonode->make_active();
        }
        $settingnode->add_node($foonode);     
   
    }
}

